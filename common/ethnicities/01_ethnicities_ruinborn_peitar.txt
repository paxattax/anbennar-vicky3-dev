﻿@neg1_min = 0.4
@neg1_max = 0.5

@neg2_min = 0.3
@neg2_max = 0.4

@neg3_min = 0.1
@neg3_max = 0.3

@pos1_min = 0.5
@pos1_max = 0.6

@pos2_min = 0.6
@pos2_max = 0.7

@pos3_min = 0.7
@pos3_max = 0.9

@beauty1min = 0.35
@beauty1max = 0.65

@beauty2min = 0.4
@beauty2max = 0.6

@beauty3min = 0.45
@beauty3max = 0.55

@blend1min = 0.0
@blend1max = 0.2

@blend2min = 0.2
@blend2max = 0.5

@blend3min = 0.5
@blend3max = 0.8

ruinborn_peitar = {
	template = "moon_elf"

	skin_color = {
		10 = { 0.0 0.05 0.15 0.1 }
		10 = { 0.5 0.1 1.0 0.25 }
	}
	hair_color = {
		# Brown
		10 = { 0.65 0.5 0.9 0.8 }
		# Red
		10 = { 0.85 0.0 0.99 0.5 }
		# Blonde
		10 = { 0.25 0.2 0.75 0.55 }
	}
}