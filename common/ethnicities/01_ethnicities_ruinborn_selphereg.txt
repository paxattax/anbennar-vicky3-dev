﻿@neg1_min = 0.4
@neg1_max = 0.5

@neg2_min = 0.3
@neg2_max = 0.4

@neg3_min = 0.1
@neg3_max = 0.3

@pos1_min = 0.5
@pos1_max = 0.6

@pos2_min = 0.6
@pos2_max = 0.7

@pos3_min = 0.7
@pos3_max = 0.9

@beauty1min = 0.35
@beauty1max = 0.65

@beauty2min = 0.4
@beauty2max = 0.6

@beauty3min = 0.45
@beauty3max = 0.55

@blend1min = 0.0
@blend1max = 0.2

@blend2min = 0.2
@blend2max = 0.5

@blend3min = 0.5
@blend3max = 0.8

ruinborn_selphereg = {
	template = "moon_elf"

	
	skin_color = {
		10 = { 0.4 0.85 0.5 0.95 } #Light Green
		10 = { 0.9 0.85 1.0 0.95 } #Pinkish White
	}
	eye_color = {
		# Brown
		10 = { 0.05 0.5 0.33 0.8 }
		# Green
		20 = { 0.33 0.5 0.67 0.8 }
		# Blue
		20 = { 0.67 0.5 1.0 0.8 }  
		#Light green
		10 = { 0.5 0.0 1.0 0.2 }
		#yellow
		10 = { 0.3 0.2 0.43 0.48 }	   
	}
	hair_color = {	
		# Greenish
		5 = { 1.0 0.3 1.0 0.45 }
		# Purple-ish
		5 = { 1.0 0.0 1.0 0.1 }
		# Yellow-ish
		5 = { 1.0 0.46 1.0 0.7 }
		# Other
		5 = { 1.0 0.0 1.0 1.0 }
	}
}

	