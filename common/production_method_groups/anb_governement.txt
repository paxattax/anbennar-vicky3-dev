pmg_base_building_archeostudies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_archeostudies_diploma
		pm_archeostudies_degree
		pm_archeostudies_masters
		pm_archeostudies_phd
	}
}

pmg_base_building_mage_tower = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_arcane_scholar_network
		pm_drudic_scholar_network
		pm_masters_of_magic
		pm_masters_of_technothaumaturgie
		pm_modern_mages
		pm_modern_technodruid
	}
}