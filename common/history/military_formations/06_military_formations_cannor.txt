﻿MILITARY_FORMATIONS = {
	c:A01 ?= { #Anbennar
		create_military_formation = {
			type = army
			hq_region = sr:region_eastern_dameshead
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DAMESEAR
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_DAMESEAR
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ISTRALORIAN_DALES
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_ISTRALORIAN_DALES
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DAMERIAN_HEARTLANDS
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_VERNE
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_VERNE
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_VERNE
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_WEX
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_WEX
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ESTALLEN
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_PEARLSEDGE
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_WESDAM
				count = 5
			}
			save_scope_as = anbennarian_army #save scope if you want to assign generals
			

		}

		create_military_formation = {
			type = army
			hq_region = sr:region_northern_dameshead
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ARBARAN
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DAMESCROWN
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_FREECESTIR
				count = 5
			}
		}
		create_military_formation = {
			type = fleet
			hq_region = sr:region_eastern_dameshead
			
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_DAMESEAR
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_PEARLSEDGE
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_NECKCLIFFE
				count = 25
			}
			
		}
	}
	c:A02 ?= { #Vivin Empire
		create_military_formation = {
			type = army
			hq_region = sr:region_the_borders
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SUGAMBER
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ARANNEN
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_ARANNEN
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_THE_LAND
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_THE_LAND
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_FENNFORT
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_RAVENGARD
				count = 10
			}
		}
		create_military_formation = {
			type = fleet
			hq_region = sr:region_the_borders
			
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_CELLIANDE
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_CELLIANDE
				count = 5
			}
		}
	}
	c:A03 ?= { #Lorent
		create_military_formation = {
			type = army
			hq_region = sr:region_south_lencenor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ROILSARD
				count = 20
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ROSECROWN
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_ROSECROWN
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BLOODWINE
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_BLOODWINE
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GREAT_ORDING
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SORNCOST
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DAROM
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DERANNE
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_DERANNE
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_CARNETER
				count = 5
			}
		}
		create_military_formation = {
			type = fleet
			hq_region = sr:region_south_lencenor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_ROSECROWN
				count = 25
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_WINEBAY
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_SORNCOST
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_DERANNE
				count = 25
			}
			
		}
	}
	c:A04 ?= { #the country of which the military formation is from
		create_military_formation = { #to create the whole military formation
			type = army #army or navy
			hq_region = sr:region_alen #strategic region where the army is

			combat_unit = { #singular combat unit
				type = unit_type:combat_unit_type_skirmish_infantry #what type of unit is it?
				state_region = s:STATE_SERPENTSHEAD  #where is this unit
				count = 10 #how many are there in this combat unit
			}

			combat_unit = {
				type = unit_type:combat_unit_type_skirmish_infantry
				state_region = s:STATE_GAWED
				count = 20
			}

			combat_unit = {
				type = unit_type:combat_unit_type_skirmish_infantry
				state_region = s:STATE_LOWER_ALEN 
				count = 10
			}

			combat_unit = {
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_FLOODMARCHES 
				count = 15
			}

			combat_unit = {
				type = unit_type:combat_unit_type_skirmish_infantry
				state_region = s:STATE_WESTMOORS_PROPER
				count = 15
			}

			combat_unit = {
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_BERONMOOR 
				count = 10
			}

			combat_unit = {
				type = unit_type:combat_unit_type_skirmish_infantry
				state_region = s:STATE_ADSHAW 
				count = 5
			}

			combat_unit = {
				type = unit_type:combat_unit_type_skirmish_infantry
				state_region = s:STATE_REACHSPIER 
				count = 25
			}

			combat_unit = {
				type = unit_type:combat_unit_type_skirmish_infantry
				state_region = s:STATE_WEST_OLAVLUND
				count = 10
			}

			save_scope_as = northern_league_army #save scope if you want to assign generals
		}

		create_military_formation = {
			type = army
			hq_region = sr:region_dragon_coast

			combat_unit = {
				type = unit_type:combat_unit_type_skirmish_infantry
				state_region = s:STATE_CODDORRAN_HEIGHTS 
				count = 5
			}

			save_scope_as = westmoors_volunteers
		}
		
		create_military_formation = {
			type = fleet
			hq_region = sr:region_the_reach

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_SERPENTSHEAD 
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_REACHSPIER 
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_WESTMOORS_PROPER 
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_REACHSPIER 
				count = 20
			}

		}
		create_character = { #Reachman Commander, lead the volunteer
			first_name = "Marcan"
			last_name = "Alencay"
			historical = yes
			culture = cu:blue_reachman
			age = 52
			interest_group = ig_trade_unions
			is_general = yes
			ideology = ideology_radical
			traits = {
				basic_offensive_planner
				direct
			}
			save_scope_as = marcan
		}
		
		create_character = { #Leader of the initial push
			first_name = "Godric"
			last_name = "Oxenham"
			historical = yes
			age = 64
			interest_group = ig_armed_forces
			is_general = yes
			ideology = ideology_jingoist_leader
			traits = {
				resupply_commander
				meticulous
			}
			save_scope_as = godric
		}

		create_character = { #Reachman Commander, would only have been 19 during the War of Blackpowder as the previous commander dies assaulting the Anb position on the Alen
			first_name = "Welyam"
			last_name = "Newport"
			historical = yes
			culture = cu:blue_reachman
			age = 38
			interest_group = ig_armed_forces
			is_general = yes
			ideology = ideology_republican_leader # He's from Celmaldor
			traits = {
				plains_commander
				ambitious
			}
			save_scope_as = welyam
		}

		create_character = { #Commander in charge of Vertesk
			first_name = "Randolph"
			last_name = "Balgard"
			historical = yes
			age = 48
			interest_group = ig_industrialists
			is_general = yes
			ideology = ideology_moderate
			traits = {
				basic_defensive_strategist
				cautious
				scarred
			}
			save_scope_as = Randolph
		}

		create_character = {
			first_name = "Tomac"
			last_name = "Woodswall"
			historical = yes
			age = 64
			culture = cu:blue_reachman
			interest_group = ig_rural_folk
			is_general = yes
			ig_leader = yes
			ideology = ideology_luddite
			traits = {
				inspirational_orator
				brave
			}
			save_scope_as = Tomac
			on_created = {
				set_variable = woodswall_leader_var
			}
		}

		scope:Tomac = {
			transfer_to_formation = scope:northern_league_army #to assign the general
		}

		scope:Randolph = {
			transfer_to_formation = scope:northern_league_army #to assign the general
		}

		scope:welyam = {
			transfer_to_formation = scope:northern_league_army #to assign the general
		}

		scope:godric = {
			transfer_to_formation = scope:northern_league_army #to assign the general
		}

		scope:marcan = {
			transfer_to_formation = scope:westmoors_volunteers #to assign the general
		}
	}
	c:A05 ?= { #Bisan
		create_military_formation = {
			type = army
			hq_region = sr:region_the_borders
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BISAN
				count = 20
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_BISAN
				count = 5
			}
		}
	}
	c:A06 ?= { #Gnomish Hierarchy
		create_military_formation = {
			type = army
			hq_region = sr:region_dragon_coast
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ODDANROY
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_THE_IONDDAMMO
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GNOMISH_PASS
				count = 10
			}
		}
		create_military_formation = {
			type = fleet
			hq_region = sr:region_dragon_coast
			
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_ODDANROY
				count = 13
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_ODDANROY
				count = 12
			}
		}
	}
	c:A07 ?= { #Reveria
		create_military_formation = {
			type = army
			hq_region = sr:region_north_lencenor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_REVERIA
				count = 10
			}
		}
		create_military_formation = {
			type = fleet
			hq_region = sr:region_north_lencenor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_REVERIA
				count = 1
			}
		}
	}
	c:A09 ?= { #Busilar
		create_military_formation = {
			type = army
			hq_region = sr:region_businor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_HAPAINE
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_KHENAK
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_KHENAK
				count = 5
			}
		}
		create_military_formation = {
			type = fleet
			hq_region = sr:region_businor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_LORIN_CANNO
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_HAPAINE
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_HAPAINE
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_VERNE
				count = 5
			}
		}
	}
	c:A10 ?= { #Grombar
		create_military_formation = {
			type = army
			hq_region = sr:region_the_reach
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_WEST_OLAVLUND
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ESALD
				count = 20
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_ESALD
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_TANNUSVALE
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_CEDESCK
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_VROREN
				count = 20
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GIANTS_TEARS
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_GIANTS_TEARS
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_GIFRBYGD
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_EAST_OLAVLUND
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MIDDLE_ALEN
				count = 20
			}
		
		}
		create_military_formation = {
			type = army
			hq_region = sr:region_northern_pass
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_HJYRTSIKKI
				count = 3
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_FRIGID_FOREST
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_COPPERWOOD
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_KHUGSVALE
				count = 10
			}
		
		}
		create_military_formation = {
			type = fleet
			hq_region = sr:region_the_reach
			
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_WEST_OLAVLUND
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_ESALD
				count = 25
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_CEDESCK
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_VROREN
				count = 5
			}
		
		}
	}
	c:A14 ?= { #Small Country
		create_military_formation = {
			type = army
			hq_region = sr:region_north_lencenor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ROYSFORT
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_CIDERFIELD
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_HOMEHILLS
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BEEPECK
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ELKMARCH
				count = 5
			}
		}
		create_military_formation = {
			type = fleet
			hq_region = sr:region_north_lencenor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_BEEPECK
				count = 3
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_BEEPECK
				count = 7
			}
		}


	}
	c:A16 ?= { #Kobildzan
		create_military_formation = {
			type = army
			hq_region = sr:region_dragon_coast
			
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_DRAGONMAW
				count = 5
			}
		}
	}
	c:A18 ?= { #Bayvek
		create_military_formation = {
			type = army
			hq_region = sr:region_the_reach
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BAYVEK
				count = 15
			}
		}
	}
	c:A19 ?= { #Vertesk
		create_military_formation = {
			type = fleet
			hq_region = sr:region_northern_dameshead
			
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_VERTESK
				count = 2
			}
		}
		create_military_formation = {
			type = army
			hq_region = sr:region_the_reach
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_VERTESK
				count = 10
			}
		}
	}
	c:A20 ?= { #Ibevar
		create_military_formation = {
			type = army
			hq_region = sr:region_adenica
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_NORTH_IBEVAR
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SOUTH_IBEVAR
				count = 10
			}
		}
	}
	c:A22 ?= { #Ancardia
		create_military_formation = {
			type = army
			hq_region = sr:region_adenica
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ADENICA
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_dragoons
				state_region = s:STATE_ADENICA
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ROHIBON
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_dragoons
				state_region = s:STATE_ROHIBON
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ANCARDIAN_PLAINS
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_ANCARDIAN_PLAINS
				count = 20
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_FARRANEAN
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MORECED
				count = 10
			}
		}
	}
	c:A25 ?= { #Araionn
		create_military_formation = {
			type = army
			hq_region = sr:region_adenica
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_OUDMERE
				count = 10
			}
		}
	}
	c:A26 ?= { #Wyvernheart
		create_military_formation = {
			type = army
			hq_region = sr:region_adenica
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SILVERVORD
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_CANNWOOD
				count = 10
			}
		}
	}
	c:A27 ?= { #Blademarches
		create_military_formation = {
			type = army
			hq_region = sr:region_the_marches
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BLADEMARCH
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_BLADEMARCH
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DEVACED
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_DOSTANS_WAY
				count = 15
			}
		}
		create_military_formation = {
			type = army
			hq_region = sr:region_the_marches
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BARUMAND
				count = 25
			}
		}
	}
	c:A28 ?= { #Rosande
		create_military_formation = {
			type = army
			hq_region = sr:region_the_marches
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ROSANVORD
				count = 15
			}
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MEDIRLEIGH
				count = 15
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BURNOLL
				count = 5
			}
		}
	}
	c:A29 ?= { #Marrhold
		create_military_formation = {
			type = army
			hq_region = sr:region_the_marches
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MARRVALE
				count = 25
			}
		}
	}
	c:A30 ?= { #Magocratic Demesne
		create_military_formation = {
			type = army
			hq_region = sr:region_castanor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_CASTONATH
				count = 30
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_NORTHYL
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_mobile_artillery
				state_region = s:STATE_STEELHYL
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_LOWER_NATH
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_WESTGATE
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_UPPER_NATH
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_EASTGATE
				count = 5
			}
		}
	}
	c:A31 ?= { #Unguldavor
		create_military_formation = {
			type = army
			hq_region = sr:region_castanor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_UNGULDAVOR
				count = 25
			}
			
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KONDUNN
				count = 25
			}
		}
	}
	c:A12 ?= { #Jazhkredu
		create_military_formation = {
			type = army
			hq_region = sr:region_castanor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BURNOLL
				count = 5
			}
		}
	}
	c:A33 ?= { #Ravelian State
		create_military_formation = {
			type = army
			hq_region = sr:region_the_borders
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ASHENIANDE
				count = 5
			}
		
		}
	}
	c:A70 ?= { #Nathalaire
		create_military_formation = {
			type = army
			hq_region = sr:region_dostanor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_CORVELD
				count = 5
			}
		
		}
		create_military_formation = {
			type = fleet
			hq_region = sr:region_dostanor
			
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_CORVELD
				count = 10
			}
		
		}
	}
	c:A79 ?= { #Cyranvar
		create_military_formation = {
			type = army
			hq_region = sr:region_deepwoods
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_LAKE_GROVE
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MOUNTAIN_GROVE
				count = 5
			}
		}
	}
	c:A80 ?= { #Soyzkaru Cartel
		create_military_formation = {
			type = army
			hq_region = sr:region_deepwoods
			
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BONE_GROVE
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ARROW_GROVE
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_OURDIA
				count = 5
			}
		}
	}
}