﻿COUNTRIES = {
	c:C47 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		
		#Kheionism
		add_technology_researched = academia
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement
		add_technology_researched = currency_standards
		add_technology_researched = banking
		add_technology_researched = colonization
		add_technology_researched = romanticism
		add_technology_researched = medical_degrees
		add_technology_researched = empiricism
		
		effect_starting_politics_traditional = yes
		
		#Laws
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_wealth_voting
		activate_law = law_type:law_state_atheism
		activate_law = law_type:law_appointed_bureaucrats
		
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_private_schools
		activate_law = law_type:law_traditional_magic_banned
		
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_migration_controls
	}
}