﻿COUNTRIES = {
	c:A15 ?= {
		effect_starting_technology_tier_1_tech = yes
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats		
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_right_of_assembly	#jokers
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_womens_suffrage # Not allowed women in workplace without voting or feminism
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_nation_of_artifice	

	}
}