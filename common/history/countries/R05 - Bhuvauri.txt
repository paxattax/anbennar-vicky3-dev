﻿COUNTRIES = {
	c:R05 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_racial_segregation #Cultural exclusion is not possible with legacy slavery
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_traditional_magic_banned
	}
}