﻿COUNTRIES = {
	c:Y17 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		effect_starting_politics_liberal = yes
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_land_based_taxation
	}
}