﻿COUNTRIES = {
	c:B23 ?= {
		effect_starting_technology_tier_2_tech = yes

		add_technology_researched = egalitarianism

		effect_starting_politics_liberal = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_cultural_exclusion #Should be multiculturalism really but not one should start with it
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_interventionism
		#activate_law = law_type:law_free_trade # INVALID - Does not exist, but if it did it'd not have the tech
		activate_law = law_type:law_per_capita_based_taxation
		# No colonial affairs
		activate_law = law_type:law_dedicated_police
		# No schools - their ideas say it's a university
		activate_law = law_type:law_no_health_system
		activate_law = law_type:law_nation_of_artifice
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		# No migration controls
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_local_tolerance

		ig:ig_intelligentsia = {
			add_ruling_interest_group = yes
		}

		ig:ig_rural_folk = {
			add_ruling_interest_group = yes
		}

		set_institution_investment_level = {
			institution = institution_police #Very efficient police as per EU4 ideas
			level = 3
		}

		add_taxed_goods = g:services
	}
}