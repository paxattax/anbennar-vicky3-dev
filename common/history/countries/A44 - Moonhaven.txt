﻿COUNTRIES = {
	c:A44 ?= {
		effect_starting_technology_tier_2_tech = yes
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia	#leftover from blackpowder rebellion
		
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_charitable_health_system
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_in_the_workplace # Women can't vote, nobody can vote
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_same_heritage_only
		
		
		activate_law = law_type:law_artifice_banned


	}
}