﻿COUNTRIES = {
	c:A11 ?= {
		effect_starting_technology_tier_3_tech = yes

		effect_starting_politics_traditional = yes
		
		# Laws 
		activate_law = law_type:law_slavery_banned
		activate_law = law_type:law_landed_voting

		ig:ig_landowners = {
			add_ruling_interest_group = yes
		}
		set_variable = {
			name = is_matriarchy_var
			value = yes
		}		
	}
}