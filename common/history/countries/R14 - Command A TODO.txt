﻿COUNTRIES = {
	c:R14 ?= {
		effect_starting_technology_tier_4_tech = yes

		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		
		effect_starting_politics_traditional = yes

		activate_law = law_type:law_stratocracy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_national_guard

		activate_law = law_type:law_debt_slavery
		
		activate_law = law_type:law_traditional_magic_banned

		add_modifier = { name = hobgoblin_statocracy_modifier }
		add_journal_entry = { type = je_command_splinter }
	}
}