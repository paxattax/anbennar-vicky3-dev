﻿COUNTRIES = {
	c:C09 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = empiricism
		
		# Laws 
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_no_home_affairs
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_religious_schools
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property # Not allowed women in workplace without voting
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_slave_trade
		
		activate_law = law_type:law_local_tolerance
		
		activate_law = law_type:law_traditional_magic_banned
	}
}