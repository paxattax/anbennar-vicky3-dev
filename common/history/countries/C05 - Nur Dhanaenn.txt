﻿COUNTRIES = {
	c:C05 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = empiricism
		add_technology_researched = stock_exchange
		
		# Laws 
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_no_home_affairs
		
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_free_trade
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_private_schools
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_local_tolerance
		
		activate_law = law_type:law_nation_of_magic
		
		add_taxed_goods = g:fruit
	}
}