﻿COUNTRIES = {
	c:F03 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_women_own_property #Women in workplace not possible without feminism
		
		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_nation_of_artifice
	}
}