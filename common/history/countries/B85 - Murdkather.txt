﻿COUNTRIES = {
	c:B85 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = lathe
		add_technology_researched = tradition_of_equality
		
		effect_starting_politics_traditional = yes
		
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_oligarchy

		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_no_health_system
		activate_law = law_type:law_women_in_the_workplace

		activate_law = law_type:law_same_heritage_only
	}
}