﻿COUNTRIES = {
	c:A05 ?= {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats #Not allowed elected with no elections
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		
		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_nation_of_artifice

		ig:ig_armed_forces = {
			add_ruling_interest_group = yes
		}
		ig:ig_landowners = {
			add_ruling_interest_group = yes
		}
	}
}