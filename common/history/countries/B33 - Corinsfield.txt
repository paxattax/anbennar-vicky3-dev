﻿COUNTRIES = {
	c:B33 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = napoleonic_warfare
		add_technology_researched = empiricism
		add_technology_researched = tradition_of_equality

		effect_starting_politics_reactionary = yes
		
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_professional_army
		# No home affairs
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_isolationism
		activate_law = law_type:law_land_based_taxation
		# No police
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_no_health_system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_womens_suffrage
		# No social security
		activate_law = law_type:law_closed_borders
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_heritage_only
		activate_law = law_type:law_traditional_magic_banned

		ig:ig_devout = {
			add_ruling_interest_group = yes
		}

		ig:ig_rural_folk = {
			add_ruling_interest_group = yes
		}

		set_state_religion = rel:corinite
	}
}