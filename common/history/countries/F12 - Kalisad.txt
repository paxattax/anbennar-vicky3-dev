﻿COUNTRIES = {
	c:F12 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		
		activate_law = law_type:law_land_based_taxation
		#activate_law = law_type:law_religious_schools
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property #Women in workplace not possible without feminism
		activate_law = law_type:law_migration_controls
		
		activate_law = law_type:law_same_heritage_only
		
		
		activate_law = law_type:law_nation_of_magic
	}
}