﻿COUNTRIES = {
	c:B27 ?= {
		effect_starting_technology_tier_2_tech = yes
		add_technology_researched = tradition_of_equality

		effect_starting_politics_reactionary = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		# No home affairs
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_frontier_colonization
		# No Police force
		# No schools
		activate_law = law_type:law_charitable_health_system
		activate_law = law_type:law_nation_of_magic
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_in_the_workplace
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_local_tolerance

		set_tariffs_import_priority = g:artillery
		set_tariffs_import_priority = g:small_arms

		set_institution_investment_level = {
			institution = institution_health_system #Nerat = medicine
			level = 1
		}	
	}
}