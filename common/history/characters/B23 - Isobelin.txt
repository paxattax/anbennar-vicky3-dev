﻿CHARACTERS = {
	c:B23 ?= {
		create_character = { #Mafia boss
			first_name = "Olor"
			last_name = Uisarn
			historical = yes
			age = 38
			interest_group = ig_petty_bourgeoisie
			ig_leader = yes
			ideology = ideology_market_liberal
			traits = {
				basic_political_operator bandit direct
			}
			culture = cu:cannorian_half_orc
		}
		create_character = { #Archprovost of Port Isobel
			first_name = "Galin"
			last_name = "Grey"
			historical = yes
			age = 46
			interest_group = ig_devout
			ig_leader = yes
			ideology = ideology_moderate
			traits = {
				arrogant
			}
		}
		create_character = { #Owner of Thornsway
			first_name = "Aldarion"
			last_name = "Seamoon"
			historical = yes
			age = 75
			interest_group = ig:ig_intelligentsia
			ig_leader = yes
			ideology = ideology_moderate
			traits = {
				opium_addiction imperious
			}
			culture = cu:redglader_elf
		}
	}
}
