﻿CHARACTERS = {
	c:B98 ?= {
		create_character = { #Marlliander who stood up to the other vampire clans and stayed loyal to the Concord
			first_name = "Willam"
			last_name = Dainuich
			historical = yes
			noble = yes
			age = 41
			ig_leader = yes
			interest_group = ig_landowners
			ideology = ideology_liberal_leader
			traits = {
				brave experienced_diplomat
			}
			culture = cu:concordian
			religion = rel:regent_court
		}
		create_character = { #Head Concord bureaucrat, created most current institutions
			first_name = "Frederic"
			last_name = "Thadoch"
			historical = yes
			ruler = yes
			age = 65
			ig_leader = yes
			interest_group = ig:ig_intelligentsia
			ideology = ideology_market_liberal
			traits = {
				master_bureaucrat imperious
			}
			culture = cu:concordian
			religion = rel:ravelian
		}
		create_character = { #Thilvisan oligarch
			first_name = "Dustin"
			last_name = "Barrows"
			historical = yes
			noble = yes
			age = 43
			interest_group = ig_industrialists
			ig_leader = yes
			ideology = ideology_slaver
			traits = {
				cruel experienced_political_operator
			}
			culture = cu:towerfoot_halfling
			religion = rel:ravelian
		}
		create_character = { #Archprovost of Port Isobel
			first_name = "Maurise"
			last_name = "Undercliffe"
			historical = yes
			age = 34
			ig_leader = yes
			interest_group = ig_devout
			ideology = ideology_abolitionist
			traits = {
				charismatic persistent
			}
			culture = cu:concordian
			religion = rel:ravelian
		}
		create_character = {
			first_name = "Tristan"
			last_name = "Ritach"
			historical = yes
			age = 39
			ig_leader = yes
			interest_group = ig_petty_bourgeoisie
			ideology = ideology_abolitionist
			traits = {
				scarred literary meticulous
			}
			culture = cu:concordian
			religion = rel:ravelian
		}
		create_character = { #Was in charge of coordinating militias pre-unification
			first_name = "Galin"
			last_name = "Grey"
			historical = yes
			age = 46
			ig_leader = yes
			interest_group = ig_armed_forces
			ideology = ideology_republican_leader
			traits = {
				arrogant
			}
			culture = cu:concordian
			religion = rel:ravelian
		}
		create_character = {
			first_name = "Evin"
			last_name = Saiste
			historical = yes
			age = 47
			ig_leader = yes
			interest_group = ig_rural_folk
			ideology = ideology_land_reformer
			traits = {
				direct
			}
			culture = cu:concordian
			religion = rel:ravelian
		}
		
		create_character = {
			first_name = "Olor"
			last_name = Uisarn
			historical = yes
			age = 38
			is_admiral = yes
			interest_group = ig_armed_forces
			ideology = ideology_moderate
			traits = {
				expert_naval_commander dockyard_master direct
			}
			culture = cu:valorborn
			religion = rel:corinite
		}
		
		create_character = {
			first_name = "Aldred"
			last_name = sil_Vis
			historical = yes
			noble = yes
			age = 56
			is_general = yes
			interest_group = ig_landowners
			ideology = ideology_market_liberal
			traits = {
				expensive_tastes arrogant
			}
			culture = cu:towerfoot_halfling
			religion = rel:ravelian
		}
		create_character = {
			first_name = "Cristof"
			last_name = "Bayman"
			historical = yes
			age = 49
			is_general = yes
			interest_group = ig_rural_folk
			ideology = ideology_pacifist
			traits = {
				shellshocked basic_artillery_commander
			}
			culture = cu:valorborn
			religion = rel:corinite
		}

		create_character = {
			first_name = "Panthes"
			last_name = "Zamuras"
			historical = yes
			age = 24
			is_admiral = yes
			interest_group = ig_intelligentsia
			ideology = ideology_pacifist
			traits = {
				charismatic convoy_raider_commander erudite
			}
			culture = cu:zanlibi
			religion = rel:old_sun_cult
		}

		create_character = { #Akal of Zanlib, in contact with eldritch friends
			first_name = "Nuriye"
			last_name = szel_Aqatzan
			historical = yes
			noble = yes
			is_agitator = yes
			female = yes
			age = 15
			interest_group = ig_rural_folk
			ideology = ideology_magocrat #to be replaced by an occult pro-evil magic one later
			traits = {
				reserved psychological_affliction
			}
			culture = cu:zanlibi
			religion = rel:old_sun_cult
		}
	}
}
