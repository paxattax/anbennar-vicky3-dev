﻿CHARACTERS = {
	c:Y04 ?= {
		create_character = {
			template = ruler_huszien
			on_created = {
				set_variable = huszien_var
				set_character_immortal = yes
			}
		}
	}
}
