﻿CHARACTERS = {
	c:A04 ?= {
		create_character = {
			first_name = "Andrew"
			last_name = "Thornham"
			historical = yes
			age = 53
			culture = cu:gawedi
			interest_group = ig_industrialists
			ruler = yes
			ideology = ideology_market_liberal
			traits = {
				direct
				wrathful
			}
		}

		create_character = {
			first_name = "Tomac"
			last_name = "Woodswall"
			historical = yes
			age = 64
			culture = cu:blue_reachman
			interest_group = ig_rural_folk
			is_general = yes
			ig_leader = yes
			ideology = ideology_luddite
			traits = {
				inspirational_orator
				brave
			}
			
			on_created = {
				set_variable = woodswall_leader_var
			}
		}
		
		create_character = {
			first_name = "Athela"
			last_name = "Drakesford"
			historical = yes
			female = yes
			age = 28
			interest_group = ig_industrialists
			ig_leader = yes
			ideology = ideology_jingoist_leader
			traits = {
				innovative
			}
		}
		
		create_character = {
			first_name = "Garret"
			last_name = "Adshaw"
			historical = yes
			age = 37
			interest_group = ig_intelligentsia
			ig_leader = yes
			ideology = ideology_republican_leader
			traits = {
				expensive_tastes
			}
		}
		
		create_character = {
			first_name = "Stovan"
			last_name = "Norleigh"
			historical = yes
			age = 44
			interest_group = ig_landowners
			ig_leader = yes
			ideology = ideology_racial_purist
			traits = {
				cruel
			}
		}
		
		create_character = {
			first_name = "Ulric"
			last_name = "Stoneberry"
			historical = yes
			culture = cu:vertesker
			age = 31
			interest_group = ig_devout
			ig_leader = yes
			ideology = ideology_republican_leader
			traits = {
				charismatic
			}
		}
		
		#Guy who pushes for the attack on Anbennar
		create_character = {
			first_name = "Brandon"
			last_name = "Jonsway"
			historical = yes
			age = 50
			interest_group = ig_armed_forces
			ig_leader = yes
			ideology = ideology_jingoist_leader
			traits = {
				alcoholic
				bigoted
			}
		}
		
		#Commander in charge of Vertesk
		create_character = {
			first_name = "Randolph"
			last_name = "Balgard"
			historical = yes
			age = 48
			interest_group = ig_industrialists
			is_general = yes
			ideology = ideology_moderate
			traits = {
				basic_defensive_strategist
				cautious
				scarred
			}
		}
		
	}
}
