﻿POPS = {
	s:STATE_AMEION = {
		region_state:C39 = {
			create_pop = {
				culture = ambhen
				size = 2664000
			}
			create_pop = {
				culture = klereyen
				size = 333000
			}
			create_pop = {
				culture = astolbhen
				size = 333000
			}
		}
	}
	
	s:STATE_EASTERN_CHENDHYA = {
		region_state:C51 = {
			create_pop = {
				culture = chendhyan
				size = 120000
			}
		}
	}
	
	s:STATE_CLEMATAR = {
		region_state:C40 = {
			create_pop = {
				culture = larankarha
				size = 1992000
			}
			create_pop = {
				culture = dugui_hi
				size = 199200
			}
			create_pop = {
				culture = advakhalla
				size = 216000
			}
		}
	}

	s:STATE_ENLARMAI = {
		region_state:C43 = {
			create_pop = {
				culture = larankarha
				size = 1520000
				split_religion = {
					larankarha = {
						oren_nayiru = 0.05
						gods_of_the_taychend = 0.95
					}
				}
			}
			create_pop = {
				culture = advakhalla
				size = 240000
				split_religion = {
					advakhalla = {
						oren_nayiru = 0.6
						gods_of_the_taychend = 0.4
					}
				}
			}
		}
		region_state:C66 = {
			create_pop = {
				culture = larankarha
				size = 200000
				split_religion = {
					larankarha = {
						oren_nayiru = 0.9
						gods_of_the_taychend = 0.1
					}
				}
			}
		}
	}

	s:STATE_IRON_HILLS = {
		region_state:C44 = {
			create_pop = {
				culture = yodhanpir
				size = 800000
			}
			create_pop = {
				culture = sarihaddi
				size = 95000
			}
		}
		region_state:C58 = {
			create_pop = {
				culture = yodhanpir
				size = 1005000
				split_religion = {
					yodhanpir = {
						oren_nayiru = 0.5
						gods_of_the_taychend = 0.5
					}
				}
			}
		}
	}

	s:STATE_NANRU_NAKAR = {
		region_state:C45 = {
			create_pop = {
				culture = kalavendhi
				size = 3900800
				split_religion = {
					kalavendhi = {
						oren_nayiru = 0.4
						gods_of_the_taychend = 0.4
						ravelian = 0.2
					}
				}
			}
		}
		region_state:C63 = {
			create_pop = {
				culture = kalavendhi
				size = 269520
				split_religion = {
					kalavendhi = {
						oren_nayiru = 0.7
						gods_of_the_taychend = 0.3
					}
				}
			}
		}
		region_state:C64 = {
			create_pop = {
				culture = kalavendhi
				size = 130800
				split_religion = {
					kalavendhi = {
						oren_nayiru = 0.8
						gods_of_the_taychend = 0.2
					}
				}
			}
		}
		region_state:C65 = {
			create_pop = {
				culture = kalavendhi
				size = 138300
			}
		}
		region_state:C43 = {
			create_pop = {
				culture = kalavendhi
				size = 121900
				split_religion = {
					kalavendhi = {
						oren_nayiru = 0.75
						gods_of_the_taychend = 0.25
					}
				}
			}
			create_pop = {
				culture = larankarha
				size = 46200
				split_religion = {
					larankarha = {
						oren_nayiru = 0.25
						gods_of_the_taychend = 0.75
					}
				}
			}
		}
	}

	s:STATE_NEOR_EMPKEIOS = {
		region_state:C60 = {
			create_pop = {
				culture = klereyen
				size = 1300000
			}
			create_pop = {
				culture = ambhen
				size = 320000
			}
		}
		region_state:C46 = {
			create_pop = {
				culture = klereyen
				size = 1200000
			}
			create_pop = {
				culture = ambhen
				size = 300000
			}
		}
	}

	s:STATE_KLERECHEND = {
		region_state:C46 = {
			create_pop = {
				culture = klereyen
				size = 1190000
			}
			create_pop = {
				culture = ambhen
				size = 228000
			}
		}
		region_state:C55 = {
			create_pop = {
				culture = klereyen
				size = 238000
			}
			create_pop = {
				culture = ambhen
				size = 40000
			}
		}
		region_state:C56 = {
			create_pop = {
				culture = klereyen
				size = 211000
			}
			create_pop = {
				culture = ambhen
				size = 50000
			}
		}
		region_state:C57 = {
			create_pop = {
				culture = klereyen
				size = 476000
			}
			create_pop = {
				culture = ambhen
				size = 60000
			}
		}
	}

	s:STATE_NYMBHAVA = {
		region_state:C46 = {
			create_pop = {
				culture = klereyen
				size = 116000
			}
		}
		region_state:C47 = {
			create_pop = {
				culture = yodhanpir
				size = 900000
				split_religion = {
					yodhanpir = {
						oren_nayiru = 0.4
						gods_of_the_taychend = 0.5
						atheist = 0.1
					}
				}
			}
			create_pop = {
				culture = klereyen
				size = 340000
			}
		}
		region_state:C58 = {
			create_pop = {
				culture = yodhanpir
				size = 920000
				split_religion = {
					yodhanpir = {
						oren_nayiru = 0.4
						gods_of_the_taychend = 0.6
					}
				}
			}
			create_pop = {
				culture = klereyen
				size = 211000
			}
		}
		region_state:C59 = {
			create_pop = {
				culture = klereyen
				size = 225000
			}
		}
	}

	s:STATE_DEGITHION = {
		region_state:C50 = {
			create_pop = {
				culture = klereyen
				size = 950200
				split_religion = {
					klereyen = {
						ravelian = 0.5
						atheist = 0.5
					}
				}
			}
			create_pop = {
				culture = thekvrystana
				size = 896800
				split_religion = {
					thekvrystana = {
						ravelian = 0.5
						atheist = 0.25
						gods_of_the_taychend = 0.25
					}
				}
			}
		}
		region_state:C61 = {
			create_pop = {
				culture = klereyen
				size = 120000
				split_religion = {
					klereyen = {
						oren_nayiru = 0.5
						atheist = 0.5
					}
				}
			}
			create_pop = {
				culture = yodhanpir
				size = 141600
				split_religion = {
					yodhanpir = {
						oren_nayiru = 0.8
						atheist = 0.2
					}
				}
			}
			create_pop = {
				culture = thekvrystana
				size = 20000
				religion = oren_nayiru
			}
		}
		region_state:A01 = {
			create_pop = {
				culture = klereyen
				size = 98000
				split_religion = {
					klereyen = {
						ravelian = 0.4
						atheist = 0.6
					}
				}
			}
			create_pop = {
				culture = pearlsedger
				size = 5600
				religion = ravelian
			}
		}
		region_state:A04 = {
			create_pop = {
				culture = klereyen
				size = 129800
				split_religion = {
					klereyen = {
						ravelian = 0.33
						atheist = 0.67
					}
				}
			}
			create_pop = {
				culture = gawedi
				size = 9000
			}
		}
	}

	s:STATE_ORENKORAIM = {
		region_state:C49 = {
			create_pop = {
				culture = kalavendhi
				size = 450300
				split_religion = {
					kalavendhi = {
						oren_nayiru = 0.2
						gods_of_the_taychend = 0.2
						ravelian = 0.6
					}
				}
			}
		}
		region_state:C48 = {
			create_pop = {
				culture = kalavendhi
				size = 898500
				split_religion = {
					kalavendhi = {
						oren_nayiru = 0.2
						ravelian = 0.8
					}
				}
			}
		}
		region_state:C62 = {
			create_pop = {
				culture = kalavendhi
				size = 224640
				split_religion = {
					kalavendhi = {
						oren_nayiru = 0.1
						gods_of_the_taychend = 0.65
						ravelian = 0.25
					}
				}
			}
		}
		region_state:C01 = {
			create_pop = {
				culture = kalavendhi
				size = 673900
				split_religion = {
					kalavendhi = {
						oren_nayiru = 0.6
						ravelian = 0.1
						new_sun_cult = 0.3
					}
				}
			}
			create_pop = {
				culture = oono_i #Replace with Dawn Ruinborn once those exist
				size = 882500
			}
			create_pop = {
				culture = rezankandish
				size = 88250
			}
			create_pop = {
				culture = dawn_elf
				size = 52950
			}
		}
		region_state:A09 = {
			create_pop = {
				culture = kalavendhi
				size = 190000
				split_religion = {
					kalavendhi = {
						oren_nayiru = 0.2
						ravelian = 0.7
						gods_of_the_taychend = 0.1
					}
				}
			}
			create_pop = {
				culture = busilari
				size = 35300
			}
		}
	}

	s:STATE_EAST_VEYII_SIKARHA = {
		region_state:C68 = {
			create_pop = {
				culture = advakhalla
				size = 696600
				religion = oren_nayiru
			}
			create_pop = {
				culture = lai_i
				size = 97200
				split_religion = {
					lai_i = {
						oren_nayiru = 0.5
						tswohvwohii = 0.5
					}
				}
			}
		}
		region_state:C63 = {
			create_pop = {
				culture = advakhalla
				size = 77400
				religion = oren_nayiru
			}
			create_pop = {
				culture = kalavendhi
				size = 18000
				religion = oren_nayiru
			}
			create_pop = {
				culture = lai_i
				size = 10800
				split_religion = {
					lai_i = {
						oren_nayiru = 0.5
						tswohvwohii = 0.5
					}
				}
			}
		}
	}

	#s:STATE_KAYDHANO_SEA = {
		#create_state = {
		#	country = C:C
		#	owned_provinces = {  }
		#}
	#}

	s:STATE_PARAIDAR = {
		region_state:C39 = {
			create_pop = {
				culture = ambhen
				size = 900000
			}
			create_pop = {
				culture = klereyen
				size = 100000
			}
		}
	}

	s:STATE_WEST_VEYII_SIKARHA = {
		region_state:C41 = {
			create_pop = {
				culture = advakhalla
				size = 310000
				split_religion = {
					advakhalla = {
						oren_nayiru = 0.1
						gods_of_the_taychend = 0.9
					}
				}
			}
			create_pop = {
				culture = dugui_hi
				size = 43000
				split_religion = {
					dugui_hi = {
						oren_nayiru = 0.5
						tswohvwohii = 0.5
					}
				}
			}
		}
		region_state:C52 = {
			create_pop = {
				culture = advakhalla
				size = 330000
				split_religion = {
					advakhalla = {
						oren_nayiru = 0.2
						gods_of_the_taychend = 0.8
					}
				}
			}
			create_pop = {
				culture = lai_i
				size = 45000
				split_religion = {
					lai_i = {
						oren_nayiru = 0.5
						tswohvwohii = 0.5
					}
				}
			}
		}
	}

	s:STATE_URVAND = {
		region_state:C42 = {
			create_pop = {
				culture = larankarha
				size = 1368000
			}
			create_pop = {
				culture = advakhalla
				size = 190000
			}
		}
		region_state:C67 = {
			create_pop = {
				culture = larankarha
				size = 342000
			}
		}
	}

	s:STATE_MESOKTI = {
		region_state:C39 = {
			create_pop = {
				culture = astolbhen
				size = 434400
			}
			create_pop = {
				culture = ambhen
				size = 552000
			}
			create_pop = {
				culture = klereyen
				size = 23000
			}
		}
		region_state:C53 = {
			create_pop = {
				culture = astolbhen
				size = 178900
			}
			create_pop = {
				culture = ambhen
				size = 76700
			}
		}
		region_state:C55 = {
			create_pop = {
				culture = astolbhen
				size = 651660
			}
			create_pop = {
				culture = ambhen
				size = 61330
			}
			create_pop = {
				culture = klereyen
				size = 207000
			}
		}
	}

	s:STATE_SARIHADDU = {
		region_state:C69 = {
			create_pop = {
				culture = sarihaddi
				size = 502000
			}
			create_pop = {
				culture = klereyen
				size = 90000
			}
		}
		region_state:C67 = {
			create_pop = {
				culture = sarihaddi
				size = 310000
				religion = oren_nayiru
			}
		}
	}

	s:STATE_CAERGARAEN = {
		region_state:C54 = {
			create_pop = {
				culture = chendhyan
				size = 935000
			}
			create_pop = {
				culture = astolbhen
				size = 165000
			}
		}
	}
    s:STATE_NORTHERN_KHEIONS = {
		region_state:C30 = {
			create_pop = {
				culture = alebhen
				size = 453900
			}
			create_pop = {
				culture = eltikani
				size = 176300
			}
		}
	}
	s:STATE_CENTRAL_KHEIONS = {
		region_state:C30 = {
			create_pop = {
				culture = alebhen
				size = 731700
		    }
		    create_pop = {
		        culture = eltikani
		        size = 38900
		    }  
		}
	}
	s:STATE_SOUTHERN_KHEIONS = {
		region_state:C30 = {
			create_pop = {
				culture = alebhen
				size = 668500
			}
			create_pop = {
				culture = eltikani
				size = 50100
			}
		}
	}
	s:STATE_SOUTIKAN = {
		region_state:C30 = {
			create_pop = {
				culture = eltikani
				size = 340600
			}
			create_pop = {
				culture = alebhen
				size = 69400
			}
		}
	}
	s:STATE_NORTIKAN = {
		region_state:C30 = {
			create_pop = {
				culture = eltikani
				size = 313000
			}
			create_pop = {
				culture = alebhen
				size = 16600
			}
		}
	}
	s:STATE_VOTHELISI_ARPENISI = {
		region_state:C30 = {
			create_pop = {
				culture = alebhen
				size = 76200
			}
		}
		region_state:C32  = {
			create_pop = {
				culture = devabhen
				size = 23500
			}
		}
		region_state:A09 = {
			create_pop = {
				culture = tefori
				size = 1100
			}
			create_pop = {
				culture = alebhen
				size = 10900
			}
		}
	}
	s:STATE_ANISIKHEION = {
		region_state:C30 = {
			create_pop = {
				culture = alebhen
				size = 15200
			}
		}
		region_state:C31 = {
			create_pop = {
				culture = devabhen
				size = 274400
			}
			create_pop = {
				culture = alebhen
				size = 81700
			}
		}
	}
	s:STATE_EMPKEIOS = {
		region_state:C32  = {
			create_pop = {
				culture = devabhen
				size = 360700
			}
			create_pop = {
				culture = vanburian
				size = 35300
			}
			create_pop = {
				culture = chendhyan
				size = 44200
			}
		}
	}
	s:STATE_KEYOLION = {
		region_state:C33 = {
			create_pop = {
				culture = devabhen
				size = 392500
			}
			create_pop = {
				culture = basobhar
				size = 129200
			}
		}
		region_state:C34 = {
			create_pop = {
				culture = devabhen
				size = 31200
			}
            create_pop = {
				culture = basobhar
				size = 16600
            }
		}
	}
	s:STATE_ENEION = {
		region_state:C34 = {
			create_pop = {
				culture = devabhen
				size = 451700
			}
			create_pop = {
				culture = basobhar
				size = 99100
			}
		}
	}
	s:STATE_BESOLAKI = {
		region_state:C35 = {
			create_pop = {
				culture = devabhen
				size = 455800
			}
			create_pop = {
				culture = basobhar
				size = 60400
			}
		}
		region_state:C34 = {
			create_pop = {
				culture = devabhen
				size = 26300
			}
            create_pop = {
                culture = basobhar
                size = 17900
            }
		}
	}
	s:STATE_APIKHOXI = {
		region_state:C36 = {
			create_pop = {
				culture = devabhen
				size = 518500
			}
			create_pop = {
				culture = basobhar
				size = 121700
			}
		}
	}
	S:STATE_AMGREMOS = {
		region_state:C70 ={
			create_pop = {
				culture = devabhen
				size = 352100
			}
			create_pop = {
				culture = ambhen
				size = 28100
			}
			create_pop = {
				culture = basobhar
				size = 24700
			}
		}
	}
	S:STATE_VOLITHORAM = {
		region_state:C70 = {
			create_pop = {
				culture = devabhen
				size = 129300
			}
			create_pop = {
				culture = basobhar
				size = 110400
			}
		}
	}
	S:STATE_OKTIAMOTON = {
		region_state:C37 = {
			create_pop = {
				culture = devabhen
				size = 80700
			}
			create_pop = {
				culture = basobhar
				size = 39100
			}
		}
	}
	S:STATE_DEYEION = {
		region_state:C37 = {
			create_pop = {
				culture = devabhen
				size = 394600
			}
			create_pop = {
				culture = ambhen
				size = 51600
			}
			create_pop = {
				culture = basobhar
				size = 23500
			}
		}
	}
	S:STATE_EASTERN_MTEIBHARA = {
		region_state:C38 = {
			create_pop = {
				culture =  mteibhar
				size = 279700
			}
			create_pop = {
				culture = basobhar
				size = 49400
			}
		}
		region_state:C37 = {
			create_pop = {
				culture = basobhar
				size = 3100
			}
			create_pop = {
				culture = devabhen
				size = 12500
			}
			create_pop = {
				culture = mteibhar
                size =  15600
			}
		}
	}
	S:STATE_WESTERN_MTEIBHARA = {
		region_state:C70 = {
			create_pop = {
				culture = mteibhar
				size = 32500
			}
			create_pop = {
				culture = devabhen
				size = 16100
			}
			create_pop = {
				culture = basobhar
				size = 5800
			}
		}
		region_state:C34 = {
			create_pop = {
				culture = devabhen
				size = 5400
			}
			create_pop = {
				culture = basobhar
				size = 4400
			}
			create_pop = {
				culture = mteibhar
				size = 6800
			}
		}
		region_state:C38 = {
			create_pop = {
				culture = basobhar
				size = 36800
			}
			create_pop = {
				culture = mteibhar
				size = 173100
			}
		}
	}
	S:STATE_WESTERN_CHENDHYA = {
		region_state:C32 = {
			create_pop = {
				culture = chendhyan
				size = 73100
			}
			create_pop = {
				culture = devabhen
				size = 41400
			}
			create_pop = {
				culture = vanburian
				size = 6100
			}
		}
	}
	S:STATE_CENTRAL_CHENDHYA = {
		region_state:C51 ={
			create_pop = {
				culture = chendhyan
				size = 130700
			}
		}
	}
}