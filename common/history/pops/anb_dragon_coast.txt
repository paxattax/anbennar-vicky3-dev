﻿POPS = {
	s:STATE_IOCHAND = {
		region_state:A15 = {
			create_pop = {
				culture = creek_gnome
				size = 343613
			}
			create_pop = {
				culture = iochander
				size = 58851
			}
			create_pop = {
				culture = redfoot_halfling
				size = 23338
				religion = regent_court
			}
			create_pop = {
				culture = cliff_gnome
				size = 13713
			}
		}
		region_state:A07 = {
			create_pop = {
				culture = reverian
				size = 628121
				split_religion = {
					reverian = {
						regent_court = 0.85
						the_thought = 0.15
					}
				}
			}
			create_pop = {
				culture = iochander
				size = 143520
			}
			create_pop = {
				culture = creek_gnome
				size = 100873
			}
			create_pop = {
				culture = cliff_gnome
				size = 17806
			}
		}
	}

	s:STATE_REVERIA = {
		region_state:A07 = {
			create_pop = {
				culture = reverian
				size = 1149080
				split_religion = {
					reverian = {
						regent_court = 0.75
						the_thought = 0.25
					}
				}
			}
			create_pop = {
				culture = bluefoot_halfling
				size = 248544
				religion = regent_court
			}
			create_pop = {
				culture = creek_gnome
				size = 60510
			}
			create_pop = {
				culture = cliff_gnome
				size = 54610
			}
		}
	}

	s:STATE_GNOMISH_PASS = {
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 689641
			}
			create_pop = {
				culture = reverian
				size = 323298
				split_religion = {
					reverian = {
						the_thought = 0.75
						regent_court = 0.25
					}
				}
			}
			create_pop = {
				culture = kobildzani_kobold
				size = 27567
				split_religion = {
					kobildzani_kobold = {
						the_thought = 0.75
						kobold_dragon_cult = 0.25
					}
				}
			}
		}
	}

	s:STATE_THE_IONDDAMMO = {
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 1041595
			}
			create_pop = {
				culture = kobildzani_kobold
				size = 1812392
				split_religion = {
					kobildzani_kobold = {
						the_thought = 0.6
						kobold_dragon_cult = 0.4
					}
				}
			}
		}
		region_state:A17 = {
			create_pop = {
				culture = cliff_gnome
				size = 420425
			}
			create_pop = {
				culture = creek_gnome
				size = 35700
			}
			create_pop = {
				culture = kobildzani_kobold
				size = 705600
				split_religion = {
					kobildzani_kobold = {
						the_thought = 0.5
						kobold_dragon_cult = 0.5
					}
				}
			}
	}

	s:STATE_ODDANROY = {
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 1113378
			}
			create_pop = {
				culture = creek_gnome
				size = 73951
			}
			create_pop = {
				culture = kobildzani_kobold
				size = 116976
				split_religion = {
					kobildzani_kobold = {
						the_thought = 0.65
						kobold_dragon_cult = 0.35
					}
				}
			}
			create_pop = {
				culture = reverian
				size = 75598
				religion = the_thought
			}
		}
	}
	
	s:STATE_DRAGONSPINE = {
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 48857
			}
			create_pop = {
				culture = kobildzani_kobold
				size = 1092433
				split_religion = {
					kobildzani_kobold = {
						kobold_dragon_cult = 0.85
						the_thought = 0.15
					}
				}
			}
		}
	}

	s:STATE_STORM_ISLES = {
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 39470
			}
		}
	}

	s:STATE_DRAGONMAW = {
		region_state:A16 = {
			create_pop = {
				culture = kobildzani_kobold
				size = 3769371	#8 million boys, just for fun
			}
		}
	}
	s:STATE_DRAGONDEPTHS = {
		region_state:A16 = {
			create_pop = {
				culture = kobildzani_kobold
				size = 4159041	#8 million boys, just for fun
			}
		}
	}
	
	s:STATE_LONELY_ISLE = {
		region_state:A03 = {
			create_pop = {
				culture = derannic
				size = 13856
			}
		}
	}
}
