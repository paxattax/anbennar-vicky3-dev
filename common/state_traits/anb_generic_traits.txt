﻿state_trait_dalairey_wastes = {	
    icon = "gfx/interface/icons/state_trait_icons/cold_climate.dds"
	
	required_techs_for_colonization = { "psychiatry" }	#the place turns people mad
	disabling_technologies = { "psionic_theory" }
	
	modifier = {
		state_non_homeland_colony_growth_speed_mult = -0.9
		state_non_homeland_mortality_mult = 0.75
	}
}

state_trait_salahad_desert = {
	icon = "gfx/interface/icons/state_trait_icons/dry_climate.dds"
	 
	modifier = {
        building_group_bg_agriculture_throughput_add = -0.2
		state_construction_mult = -0.25
		state_infrastructure_mult = -0.25
    }
}

state_trait_magical_land = {
    icon = "gfx/interface/icons/state_trait_icons/good_soils.dds"

    modifier = {
        goods_output_magical_reagents_mult = 0.25
    }
}

state_trait_magical_land_lesser = {
    icon = "gfx/interface/icons/state_trait_icons/good_soils.dds"

    modifier = {
        goods_output_magical_reagents_mult = 0.10
    }
}