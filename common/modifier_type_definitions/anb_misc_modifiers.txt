﻿country_mages_pol_str_mult={
	decimals=1
	color=neutral
	percent=yes
	game_data={
		ai_value=0
	}
}

country_adventurers_pol_str_mult={
	decimals=1
	color=neutral
	percent=yes
	game_data={
		ai_value=0
	}
}

country_soldiers_pol_str_mult={
	decimals=1
	color=neutral
	percent=yes
	game_data={
		ai_value=0
	}
}

building_group_bg_agriculture_goods_output_magical_reagents_mult = {
	decimals=1
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

country_pollution_generation_mult = {
	decimals=1
	color=bad
	percent=yes
	game_data={
		ai_value=0
	}
}

state_food_security_mult={
	decimals=2
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}

market_land_trade_capacity_mult={
	decimals=2
	color=good
	percent=yes
	game_data={
		ai_value=0
	}
}