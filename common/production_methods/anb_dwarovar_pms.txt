﻿

#Holds
pm_hold_simple = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			# output goods
			goods_output_iron_add = 20
			goods_output_clothes_add = 5
			goods_output_furniture_add = 5
		}

		level_scaled = {
			building_employment_laborers_add = 6500
			building_employment_clerks_add = 1500
			building_employment_bureaucrats_add = 1000
		}
	}
}
pm_hold_developing = {
	texture = "gfx/interface/icons/production_method_icons/pumps.dds"

	unlocking_technologies = {
		mechanical_tools
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_hardwood_add = 15
			# output goods
			goods_output_iron_add = 10
			goods_output_steel_add = 10
			goods_output_clothes_add = 10
			goods_output_furniture_add = 10
		}

		level_scaled = {
			building_employment_laborers_add = 6000
			building_employment_machinists_add = 500
			building_employment_clerks_add = 1000
			building_employment_shopkeepers_add = 500
			building_employment_bureaucrats_add = 1000
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_from_population_max_add = 2.5
		}
	}
}
pm_hold_established = {
	texture = "gfx/interface/icons/production_method_icons/condensing_engine_pump.dds"

	unlocking_technologies = {
		mechanized_workshops
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_hardwood_add = 5
			goods_input_iron_add = 15
			# output goods
			goods_output_steel_add = 30
			goods_output_luxury_clothes_add = 5
			goods_output_luxury_furniture_add = 5
		}

		level_scaled = {
			building_employment_laborers_add = 5000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 500
			building_employment_clerks_add = 500
			building_employment_shopkeepers_add = 1000
			building_employment_bureaucrats_add = 1000
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_from_population_max_add = 5
		}
	}
}
pm_hold_advanced = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		shift_work
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_iron_add = 40
			# output goods
			goods_output_steel_add = 60
			goods_output_luxury_clothes_add = 10
			goods_output_luxury_furniture_add = 10
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 1000
			building_employment_shopkeepers_add = 1500
			building_employment_bureaucrats_add = 1000
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_from_population_max_add = 7.5
		}
	}
}

pm_hold_passages = {
	texture = "gfx/interface/icons/production_method_icons/simple_organization.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			# output goods
			goods_output_transportation_add = 20
		}

		level_scaled = {
			building_employment_laborers_add = 1000
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 30
		}
	}
}
pm_hold_pulleys = {
	texture = "gfx/interface/icons/production_method_icons/horizontal_drawer_cabinets.dds"

	unlocking_technologies = {
		shaft_mining
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_wood_add = 20
			# output goods
			goods_output_transportation_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 750
			building_employment_machinists_add = 250
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 40
		}
	}
}
pm_hold_dagrite_ele = {
	texture = "gfx/interface/icons/production_method_icons/vertical_filing_cabinets.dds"

	unlocking_technologies = {
		watertube_boiler
	}

	unlocking_production_methods = {
		pm_hold_established
		pm_hold_advanced
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_iron_add = 10
			goods_input_magical_reagents_add = 10
			# output goods
			goods_output_transportation_add = 50
		}

		level_scaled = {
			building_employment_laborers_add = 500
			building_employment_machinists_add = 350
			building_employment_engineers_add = 150
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 50
		}
	}
}
pm_hold_electric_ele = {
	texture = "gfx/interface/icons/production_method_icons/telephone_switchboards.dds"

	unlocking_technologies = {
		electrical_generation
	}

	unlocking_production_methods = {
		pm_hold_established
		pm_hold_advanced
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_electricity_add = 20
			# output goods
			goods_output_transportation_add = 70
		}

		level_scaled = {
			building_employment_laborers_add = 250
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 60
		}
	}
}

pm_hold_no_spec = {
	texture = "gfx/interface/icons/production_method_icons/no_automation.dds"
	is_default = yes

	timed_modifiers = {
		dwarven_hold_specialization_change_modifier
	}

	ai_weight = 0
}
pm_hold_farm = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"

	building_modifiers = {
		level_scaled = {
			building_employment_laborers_add = -1000
			building_employment_farmers_add = 1000
		}
	}

	state_modifiers = {
		workforce_scaled = {
			building_group_bg_agriculture_throughput_add = 0.01
			building_group_bg_ranching_throughput_add = 0.01
			building_group_bg_plantations_throughput_add = 0.01
			building_food_industry_throughput_add = 0.01
		}
	}

	timed_modifiers = {
		dwarven_hold_specialization_change_modifier
	}

	ai_weight = 0
}
pm_hold_foundry = {
	texture = "gfx/interface/icons/production_method_icons/bessemer_process.dds"

	building_modifiers = {
		level_scaled = {
			building_employment_laborers_add = -1000
			building_employment_machinists_add = 500
			building_employment_engineers_add = 500
		}
	}

	state_modifiers = {
		workforce_scaled = {
			building_steel_mills_throughput_add = 0.01
			building_tooling_workshops_throughput_add = 0.01
		}
	}

	timed_modifiers = {
		dwarven_hold_specialization_change_modifier
	}

	ai_weight = 0
}
pm_hold_artisan = {
	texture = "gfx/interface/icons/production_method_icons/craftsman_sewing.dds"

	building_modifiers = {
		level_scaled = {
			building_employment_laborers_add = -1000
			building_employment_shopkeepers_add = 1000
		}
	}

	state_modifiers = {
		workforce_scaled = {
			building_textile_mills_throughput_add = 0.01
			building_furniture_manufacturies_throughput_add = 0.01
			building_glassworks_throughput_add = 0.01
		}
	}

	timed_modifiers = {
		dwarven_hold_specialization_change_modifier
	}

	ai_weight = 0
}
pm_hold_military = {
	texture = "gfx/interface/icons/production_method_icons/squad_infantry.dds"

	building_modifiers = {
		level_scaled = {
			building_employment_laborers_add = -1000
			building_employment_soldiers_add = 750
			building_employment_officers_add = 250
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_building_barracks_max_level_add = 5
			goods_input_small_arms_mult = -0.01
			goods_input_ammunition_mult = -0.01
			goods_input_artillery_mult = -0.01
			goods_input_tanks_mult = -0.01
		}
	}

	timed_modifiers = {
		dwarven_hold_specialization_change_modifier
	}
	
	ai_weight = 0
}

pm_hold_no_auto = {
	texture = "gfx/interface/icons/production_method_icons/no_automation.dds"
}
pm_hold_vents = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"

	unlocking_technologies = {
		mechanical_tools
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			# output goods
		}

		level_scaled = {
			building_employment_laborers_add = -1500
		}
	}
}
pm_hold_spells = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"

	unlocking_technologies = {
		mechanized_workshops
	}

	unlocking_production_methods = {
		pm_hold_developing
		pm_hold_established
		pm_hold_advanced
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_magical_reagents_add = 5
			# output goods
		}

		level_scaled = {
			building_employment_laborers_add = -2000
			building_employment_machinists_add = -500
		}
	}
}
pm_hold_doors = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"

	unlocking_technologies = {
		thinking_servos
	}

	unlocking_production_methods = {
		pm_hold_established
		pm_hold_advanced
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_artificery_doodads_add = 5
			# output goods
		}

		level_scaled = {
			building_employment_laborers_add = -2500
			building_employment_machinists_add = -1000
		}
	}
}
pm_hold_control = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"

	unlocking_technologies = {
		artificial_life
	}

	unlocking_production_methods = {
		pm_hold_established
		pm_hold_advanced
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_artificery_doodads_add = 5
			goods_input_electricity_add = 5
			# output goods
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1000
			building_employment_shopkeepers_add = -500
		}
	}
}

#Dwarovrod
pm_dwarovrod_minecarts = {
	texture = "gfx/interface/icons/production_method_icons/experimental_trains.dds"

	building_modifiers = {
		workforce_scaled = {
			goods_output_transportation_add = 20
		}

		level_scaled = {
			building_employment_laborers_add = 4000
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 20
			market_land_trade_capacity_add = 2.5
		}
	}
}
pm_dwarovrod_restored_trains = {
	texture = "gfx/interface/icons/production_method_icons/trains_steam.dds"

	unlocking_technologies = {
		atmospheric_engine
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_coal_add = 3
			goods_input_engines_add = 4

			goods_output_transportation_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 250
			building_employment_clerks_add = 750
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 30
			state_pollution_generation_add = 25
			market_land_trade_capacity_add = 5
		}
	}
}
pm_dwarovrod_runic_engines = {
	texture = "gfx/interface/icons/production_method_icons/trains_electric.dds"

	unlocking_technologies = {
		elemental_elicitation
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_magical_reagents_add = 5
			goods_input_engines_add = 4

			goods_output_transportation_add = 40
		}

		level_scaled = {
			building_employment_laborers_add = 2500
			building_employment_machinists_add = 750
			building_employment_clerks_add = 750
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 40
			state_pollution_generation_add = 25
			market_land_trade_capacity_add = 7.5
		}
	}
}
pm_dwarovrod_artifice_engines = {
	texture = "gfx/interface/icons/production_method_icons/trains_diesel.dds"

	unlocking_technologies = {
		magic_wave_stabilization
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_artificery_doodads_add = 6
			goods_input_engines_add = 3

			goods_output_transportation_add = 50
		}

		level_scaled = {
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 250
			building_employment_clerks_add = 750
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 50
			state_pollution_generation_add = 15
			market_land_trade_capacity_add = 10
		}
	}
}

pm_dwarovrod_basic_restoration = {
	texture = "gfx/interface/icons/production_method_icons/no_passenger_trains.dds"

	building_modifiers = {
		level_scaled = {
			building_employment_laborers_add = 1000
		}
	}
}
pm_dwarovrod_dagrite = {
	texture = "gfx/interface/icons/production_method_icons/passenger_trains.dds"

	unlocking_technologies = {
		bessemer_process
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_iron_add = 3

			goods_output_transportation_add = 20
		}

		level_scaled = {
			building_employment_laborers_add = 750
			building_employment_machinists_add = 250
		}
	}

	state_modifiers = {
		workforce_scaled = {
			market_land_trade_capacity_add = 2.5
		}
	}
}
pm_dwarovrod_mithril = {
	texture = "gfx/interface/icons/production_method_icons/passenger_carriages.dds"

	unlocking_technologies = {
		open_hearth_process
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_flawless_metal_add = 3

			goods_output_transportation_add = 40
		}

		level_scaled = {
			building_employment_laborers_add = 500
			building_employment_machinists_add = 500
		}
	}

	state_modifiers = {
		workforce_scaled = {
			market_land_trade_capacity_add = 5
		}
	}
}

pm_dwarovrod_no_auto = {
	texture = "gfx/interface/icons/production_method_icons/no_automation.dds"
}
pm_dwarovrod_golems_basic = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"

	unlocking_technologies = {
		punch_card_artificery
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_magical_reagents_add = 6
		}

		level_scaled = {
			building_employment_laborers_add = -1500
		}
	}
}
pm_dwarovrod_golems_assistants = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"

	unlocking_technologies = {
		early_mechanim
	}

	unlocking_production_methods = {
		pm_dwarovrod_runic_engines
		pm_dwarovrod_artifice_engines
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_magical_reagents_add = 9
		}

		level_scaled = {
			building_employment_laborers_add = -1500
			building_employment_machinists_add = -500
		}
	}
}
pm_dwarovrod_golems_conductors = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"

	unlocking_technologies = {
		advanced_mechanim
	}

	unlocking_production_methods = {
		pm_dwarovrod_runic_engines
		pm_dwarovrod_artifice_engines
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_magical_reagents_add = 12
		}

		level_scaled = {
			building_employment_laborers_add = -1500
			building_employment_machinists_add = -750
			building_employment_clerks_add = -250
		}
	}
}