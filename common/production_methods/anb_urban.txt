﻿#These need to be redone anyways afaik


#This needs to become the equivalent of different artists, rather than shares as thats moved to ownership buildings
pm_independent_mages = {
	texture = "gfx/interface/icons/production_method_icons/independent_mages.dds"
	
	disallowing_laws = {
		law_command_economy
	}

	building_modifiers = {
		level_scaled = {
			building_employment_mages_add = 100
		}
		# unscaled = {
		# 	building_mages_shares_add = 5
		# }
	}
}

pm_religious_mages = {
	texture = "gfx/interface/icons/production_method_icons/religious_mages.dds"
	
	disallowing_laws = {
		law_command_economy
		law_total_separation
	}

	building_modifiers = {
		level_scaled = {
			building_employment_clergymen_add = 100
		}
		# unscaled = {
		# 	building_clergymen_shares_add = 5
		# }
	}
}