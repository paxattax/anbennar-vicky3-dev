﻿building_mage_tower = {
	building_group = bg_mage_tower
	icon = "gfx/interface/icons/building_icons/mage_academies.dds"
	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_monuments.dds"
	city_type = city
	levels_per_mesh = 5
	
	# Commented out because the effect giving traditions at game start meant that no one could actually start with the PMs active. Re-enable these requirements once these techs are unlocked at game start rather than effect!
	# unlocking_technologies = { 
	# 	cannorian_magical_tradition
	# 	runecarving_magical_tradition
	# 	fourknowing_magical_tradition
	# 	essence_magical_tradition
	# 	chi_cultivation_magical_tradition
	# 	druidic_magical_tradition
	# 	shamanism_magical_tradition
	# 	bardic_magical_tradition
	# 	precursor_magical_tradition 
	# }

	production_method_groups = {
		pmg_base_building_mage_tower
	}
	
	required_construction = construction_cost_low
}
