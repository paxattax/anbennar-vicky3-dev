﻿test = {
	texture = "gfx/interface/icons/pops_icons/capitalists.dds"
	color = hsv{ 0.52 0.32 0.49 }
	strata = poor
	start_quality_of_life = 5
	wage_weight = 1
	literacy_target = 0.0
	dependent_wage = 0.5	# per year
	unemployment = yes
	unemployment_wealth = 5
	can_always_hire = no
	
	# 70% - 100% politically engaged
	political_engagement_base = 0
	political_engagement_literacy_factor = 0.1
	
	political_engagement_mult = {
		value = 1.0

		add = {
			desc = "POP_STARVATION"	
			
			if = {
				limit = { 
					standard_of_living < 5
				}
				value = 1
			}		
		}

		multiply = {
			desc = "NO_LABOR_MOVEMENT"	
			value = 1.0
			
			if = {
				limit = { 
					owner = {
						NOT = { has_technology_researched = labor_movement }
					}
				}
				value = 0.5
			}		
		}
		
		multiply = {
			desc = "HAS_POLITICAL_AGITATION"	
			value = 1.0
			
			if = {
				limit = { 
					owner = {
						has_technology_researched = political_agitation
					}
				}
				value = 1.5
			}		
		}		
	}
	
	qualifications = {
        if = {
            limit = {
                has_pop_culture = mechanim
                }
            value = 1
        if = {
        	limit = {
        		not = {has_pop_culture = mechanim}
        	}
        	value = 0
        }
    }
  }
	
	portrait_age = {
		integer_range = {
			min = define:NPortrait|GRACEFUL_AGING_START
			max = define:NPortrait|GRACEFUL_AGING_END
		}
	}
	portrait_pose = { value = 0 }			
	portrait_is_female = { always = yes }
}