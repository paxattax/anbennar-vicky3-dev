﻿elikhetist = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		khetist_religion
	}
	color = { 252 162 100 }
}

nirakhetist = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		khetist_religion
	}
	color = { 36 131 194 }
}

aakhetist = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		khetist_religion
	}
	color = { 112 74 7 }
}




xhazobkult = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		gnollish_religion
	}
	color = { 181 230 29 }
}
kvangahga = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		gnollish_religion
	}
	color = { 80 22 22 }
}


mother_akasik = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		tanibic_religion
	}
	color = { 200 162 100 }
}
noukahi_pantheon = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		tanibic_religion
	}
	color = { 198 110 30 }
}
fangaulan_pantheon = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		tanibic_religion
	}
	color = { 79 91 137 }
}
nakavy_avo = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		tanibic_religion
	}
	color = { 183 110 121 }
}


anzalkatsa = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		lizardfolk_religion
	}
	color = { 104 155 55 }
}
zaradyrra = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		lizardfolk_religion
	}
	color = { 211 41 58 }
}



beast_memory = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		east_sarhal_religion
	}
	color = { 222 80 46 }
}
sky_domain = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		east_sarhal_religion
	}
	color = { 150 180 250 }
}
rinta_seekers = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		east_sarhal_religion
	}
	color = { 0 99 99 }
}
habab_zar = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		east_sarhal_religion
	}
	color = { 91 43 85 }
}
jinnisadda = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		east_sarhal_religion
	}
	color = { 50 50 200 }
}


night_coven = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		shadow_plane_religion
	}
	color = { 6  6  6 }
}
shadow_pact = {
	texture = "gfx/interface/icons/religion_icons/catholic.dds"
	traits = {
		shadow_plane_religion
	}
	color = { 10 4 30 }
}
