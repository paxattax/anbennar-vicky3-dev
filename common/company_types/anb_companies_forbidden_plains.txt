﻿company_garsvulshim_conglomerate = {
	icon = "gfx/interface/icons/company_icons/basic_agriculture_1.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_farm_wheat.dds"

	flavored_company = yes

	building_types = {
		building_wheat_farm
		building_rye_farm
		building_livestock_ranch
	}

	potential = {
		c:EO2 ?= this
	}

	possible = {
		c:E02 ?= this
		has_law = law_type:law_state_religion
	}

	prosperity_modifier = {
		building_food_industry_throughput_add = 0.15
		state_loyalists_from_sol_change_mult = 0.05
		interest_group_ig_devout_pol_str_mult = 0.2
	}

	ai_weight = {
		value = 3
	}
}

company_olkhaljukh_heavy_industries = {
	icon = "gfx/interface/icons/company_icons/basic_steel.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_heavy.dds"

	flavored_company = yes

	building_types = {
		building_iron_mine
		building_coal_mine
		building_steel_mills
	}

	potential = {
		c:EO2 ?= this
	}

	possible = {
		c:E02 ?= this
		any_scope_building = {
			is_building_type = building_iron_mine
			level >= 3
		}
		any_scope_building = {
			is_building_type = building_coal_mine
			level >= 3
		}
	}

	prosperity_modifier = {
		building_motor_industry_throughput_add = 0.1
		interest_group_ig_industrialists_pol_str_mult = 0.15
	}

	ai_weight = {
		value = 3
	}	
}

company_spiderbite_longtooth_meatgut = {
	icon = "gfx/interface/icons/company_icons/basic_munitions.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_heavy.dds"

	flavored_company = yes

	building_types = {
		building_explosives_factory
		building_artillery_foundries
		building_munition_plants
	}

	potential = {
		has_interest_marker_in_region = region_north_plains
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_DZIMOKLI
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_VERNE
			any_scope_building = {
				is_building_type = building_artillery_foundries
				level >=2
			}
		}
		has_technology_researched = breech_loading_artillery
	}

	prosperity_modifier = {
		unit_combat_unit_type_siege_artillery_offense_mult= 0.1
		unit_kill_rate_add = 0.1 
	}

	ai_weight = {
		value = 3
	}
}