﻿company_alain_fruit = { #allain & sons fruit company
	icon = "gfx/interface/icons/company_icons/basic_colonial_plantations_2.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_plantation.dds"
	
	flavored_company = yes
	
	building_types = {  
		building_banana_plantation
		building_sugar_plantation
		building_maize_farm
	}	
	
	potential = {
		country_has_primary_culture = cu:dhanaenno
	}
	
	possible = {
		any_scope_state = {
			state_is_in_south_aelantir = yes
			any_scope_building = {
				is_building_type = building_banana_plantation
				level >= 5
			}
		}
	}
	
	prosperity_modifier = {
		building_group_bg_plantations_throughput_add = 0.2
		country_infamy_decay_mult = -0.25
	}	
	
	ai_weight = {
		value = 3 # Higher base value for flavored companies
	}
}

company_holuka = {
	icon = "gfx/interface/icons/company_icons/basic_food.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_light.dds"

	flavored_company = yes

	building_types = {
		building_sugar_plantation
		building_maize_farm
		building_food_industry
	}

	potential = {
		has_interest_marker_in_region = region_soruin
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = S:STATE_YARUHOL
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = S:STATE_YARUHOL
			is_incorporated = yes
			any_scope_building = {
				is_building_type = building_food_industry
				level >=4
			}
		}
	}

	prosperity_modifier = {
		state_birth_rate_mult = 0.05
		state_loyalists_from_sol_change_mult = 0.1
	}

	ai_weight = {
		value = 3 # Higher base value for flavored companies
	}
}

company_frostvine_alcopops = {
	icon = "gfx/interface/icons/company_icons/basic_food.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_light.dds"

	flavored_company = yes

	building_types = {
		building_sugar_plantation
		building_food_industry
		building_vineyard_plantation
	}

	potential = {
		has_interest_marker_in_region = region_devand
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_KEYOLION
			}
			any_scope_state = {
				state_region = s:STATE_ENEION
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_KEYOLION
			is_incorporated = yes
			any_scope_building = {
				is_building_type = building_food_industry
				level >=5
			}
		}
		any_scope_state = {
			state_region = s:STATE_ENEION
			is_incorporated = yes
			any_scope_building = {
				is_building_type = building_vineyard_plantation
				level >=3
			}
		}
	}

	prosperity_modifier = {
		state_birth_rate_mult = 0.05
		country_prestige_mult = 0.1
	}

	ai_weight = {
		value = 3 # Higher base value for flavored companies
	}
}

company_waenofah = {           # waenofah deep mining conglomerate
	icon = "gfx/interface/icons/company_icons/basic_metal_mining.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_mining.dds"

	flavored_company = yes

	building_types = {
		building_iron_mine
		building_explosives_factory
	}

	potential = {
		has_interest_marker_in_region = region_devand
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_KEYOLION
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_KEYOLION
			is_incorporated = yes
			any_scope_building = {
				is_building_type = building_iron_mine
				level >= 3
			}
		}
		has_technology_researched = nitroglycerin
	}

	prosperity_modifier = {
		building_steel_mills_throughput_add = 0.1
		building_sulfur_mine_throughput_add = 0.1
	}

	ai_weight = {
		value = 3
	}
}

company_mteibas_earthshapers = { #Mteibas earthshapers blasting consortium
	icon = "gfx/interface/icons/company_icons/basic_steel.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_heavy.dds"

	flavored_company = yes

	building_types = {
		building_iron_mine
		building_steel_mills
		building_magical_reagents_workshop
	}

	potential = {
		country_has_primary_culture = cu:mteibhar
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_EASTERN_MTEIBHARA
			is_incorporated = yes
			any_scope_building = {
				is_building_type = building_steel_mills
				level >= 3
			}
			any_scope_building = {
				is_building_type = building_iron_mine
				level >= 3
			}
		}
		country_has_primary_culture = cu:mteibhar
	}

	prosperity_modifier = {
		state_construction_mult = 0.05
		country_mages_pol_str_mult = 0.15
	}

	ai_weight = {
		value = 3
	}
}

company_north_andeios_logistics = { #company Andeios Logistics
	icon = "gfx/interface/icons/company_icons/basic_shipyards.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_railways.dds"

	flavored_company = yes

	building_types = {
		building_shipyards
		building_railway
	}

	potential = {
		has_interest_marker_in_region = region_devand
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_EMPKEIOS
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_EMPKEIOS
			is_incorporated = yes
			any_scope_building = {
				is_building_type = building_shipyards
				level >= 3
			}
			any_scope_building = {
				is_building_type = building_port
				level >= 5
			}
		}
		custom_tooltip = {
			text = chendhyan_frontier_required_tt
			any_scope_state = {
				state_region = s:STATE_EMPKEIOS
				has_modifier = chendhyan_gateway_to_taychend
			}
		}
	}

	prosperity_modifier = {
		state_building_port_max_level_add = 3
		market_land_trade_capacity_add = 15
	}

	ai_weight = {
		value = 3
	}
}

company_axehead_krakeneerers = { #company axehead grand krakeneerers
	icon = "gfx/interface/icons/company_icons/basic_fishing.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_farm_corn.dds"

	flavored_company = yes

	building_types = {
		building_shipyards
		building_whaling_station
		building_fishing_wharf
	}

	potential = {
		has_interest_marker_in_region = region_devand
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_AMGREMOS
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_AMGREMOS
			is_incorporated = yes
			OR = {
				any_scope_building = {
					is_building_type = building_shipyards
					level >= 3
				}
				any_scope_building = {
					is_building_type = building_fishing_wharf
					level >= 3
				}
				any_scope_building = {
					is_building_type = building_whaling_station
					level >= 3
				}
			}
		}
	}

	prosperity_modifier = {
		country_prestige_from_navy_power_projection_mult = 0.25
		unit_navy_offense_mult = 0.05
		country_adventurers_pol_str_mult = 0.5
	}

	ai_weight = {
		value = 3
	}
}

company_devand_harmony_looms = { #devand harmony looms
	icon = "gfx/interface/icons/company_icons/basic_textiles.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_light.dds"

	flavored_company = yes

	building_types = {
		building_vineyard_plantation
		building_textile_mills
		building_paper_mills
	}

	potential = {
		has_interest_marker_in_region = region_devand
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_ENEION
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_ENEION
			is_incorporated = yes
			OR = {
				any_scope_building = {
					is_building_type = building_textile_mills
					level >= 3
				}
				any_scope_building = {
					is_building_type = building_paper_mills
					level >= 3
				}
			}
			any_scope_building = {
				is_building_type = building_vineyard_plantation
				level >= 3
			}
		}
	}

	prosperity_modifier = {
		country_influence_mult = 0.1
		state_assimilation_mult = 0.25
		country_radicals_from_conquest_mult = -0.1
	}

	ai_weight = {
		value = 3
	}
}

company_kheionic_artificery = { #society for kheionic artificery
	icon = "gfx/interface/icons/company_icons/basic_metalworks.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_manufacturing_heavy.dds"

	flavored_company = yes

	building_types = {
		building_automatories
		building_tooling_workshops
	}
	potential = {
		has_interest_marker_in_region = region_alecand
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_CENTRAL_KHEIONS
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_CENTRAL_KHEIONS
			is_incorporated = yes
			any_scope_building = {
				is_building_type = building_tooling_workshops
				level >=3
			}
			any_scope_building = {
				is_building_type = building_automatories
				level >=3
			}
		}
		has_technology_researched = corporate_charters
		NOT = {
			has_law = law_type:law_isolationism
		}
	}

	prosperity_modifier = {
		country_tech_spread_mult = 0.15
		 interest_group_ig_landowners_approval_add = -4 
	}

	ai_weight = {
		value = 3
	}
}

company_amadian_precious_goods = { # Amadian Precious Goods Company
	icon = "gfx/interface/icons/company_icons/basic_gold_mining.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_mining.dds"

	flavored_company = yes 

	building_types = {
		building_damestear_mine
		building_gold_mine
	}

	potential = {
		has_interest_marker_in_region = region_amadia
	}
	
	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_OOMU_NELIR
			}
			any_scope_state = {
				state_region = s:STATE_ARANLAS
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_OOMU_NELIR
			any_scope_building = {
				is_building_type = building_gold_mine 
				level >= 3
			}
		}
		any_scope_state = {
			state_region = s:STATE_ARANLAS
			any_scope_building = {
				is_building_type = building_damestear_mine
				level >= 3
			}
		}
	}

	prosperity_modifier = {
		state_loyalists_from_sol_change_mult = 0.1
		country_prestige_mult = 0.1
	}

	ai_weight = {
		value = 3
	}
}

company_ozgarom_deforestation_self = { #OZGAROM DEFORESTATION AND SELF-DEFENCE
	icon = "gfx/interface/icons/company_icons/basic_forestry.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_farm_corn.dds"

	flavored_company = yes

	building_types = {
		building_arms_industry
		building_logging_camp
	}

	potential = {
		has_interest_marker_in_region = region_soruin
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_OZGAR
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_OZGAR
			any_scope_building = {
				is_building_type = building_arms_industry
				level >= 1
			}
		}
		any_scope_state = {
			any_scope_building = {
				is_building_type = building_logging_camp
				level >=4
			}
			count >=4
		}
	}

	prosperity_modifier = {
		state_colony_growth_speed_mult = 0.1
		unit_army_offense_mult = 0.05
		state_mortality_mult= 0.05
	}

	ai_weight = {
		value = 3
	}
}